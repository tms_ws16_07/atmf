package pictures.taking.atmfinder;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.osmnavigator.MapActivity;

import org.osmdroid.bonuspack.location.POI;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

/**
 * Created by cy on 15.01.2017.
 */

public class AtmListAdapter extends BaseAdapter {

    private Context mContext;
    private MapActivity mapActivity;
    private MapView map;


    public AtmListAdapter(Context context, MapActivity mapAct) {
        this.mContext = context;
        this.mapActivity = mapAct;
    }


    @Override
    public int getCount() {
        if (MapActivity.mPOIs == null)
            return 0;
        else
            return MapActivity.mPOIs.size();
    }

    @Override
    public Object getItem(int i) {
        if (MapActivity.mPOIs == null)
            return null;
        else
            return MapActivity.mPOIs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
//       View meineZeile = this.layoutInflater.inflate(R.layout.meine_zeile,null);
        final POI entry = (POI) getItem(i);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.meine_zeile, viewGroup, false);
        }

//        Atm atm = (Atm)MapActivity.mPOIs.get(i);
        ImageView iv = (ImageView) convertView.findViewById(R.id.thumbnail);
        iv.setImageResource(R.drawable.marker_poi);

        TextView tvTitle = (TextView) convertView.findViewById(R.id.title);
        tvTitle.setText(entry.mType);

        TextView tvDetails = (TextView) convertView.findViewById(R.id.details);
        if (!entry.mDescription.isEmpty()) {
            tvDetails.setVisibility(View.VISIBLE);
            tvDetails.setText(entry.mDescription);
        } else {
            tvDetails.setVisibility(View.GONE);
        }

        TextView tvAddress = (TextView) convertView.findViewById(R.id.address);
        tvAddress.setText(entry.mAddress);

        ImageView iv2 = (ImageView) convertView.findViewById(R.id.routeMeto);
        iv2.setImageResource(R.drawable.route_me);

        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View searchPanel = mapActivity.findViewById(R.id.atm_list_view);
                //searchPanel.clearAnimation();
                searchPanel.setVisibility(View.GONE);
               // searchPanel.setActivated(false);
               // searchPanel.setEnabled(false);
               // searchPanel.clearAnimation();
               //if( searchPanel.getVisibility() == View.GONE ) {

                   map = (MapView) mapActivity.findViewById(R.id.map);
                  // map.invalidate();

                   mapActivity.setDestinationPoint(entry.mLocation);
                   mapActivity.setMarkerDestination(mapActivity.updateItineraryMarker(mapActivity.getMarkerDestination(), mapActivity.getDestinationPoint(), MapActivity.DEST_INDEX,
                           pictures.taking.atmfinder.R.string.destination, R.drawable.marker_poi, -1, null));
                   mapActivity.getRoadAsync();


                   InfoWindow.closeAllInfoWindowsOn(map);
                   Toast.makeText(v.getContext(), R.string.MapActivity_calculate_route, Toast.LENGTH_LONG).show();

                  // map.invalidate();
//                map.getController().animateTo(mapActivity.);
//                map.getController().setZoom(18);
                   //            map.getController().setCenter(mapActivity.getStartPointPoint());


                   //map.getController().animateTo(entry.mLocation);
                   final Handler handler = new Handler();
                   handler.postDelayed(new Runnable() {
                       @Override
                       public void run() {
                           // Do something after 5s = 5000ms
                           map.getController().animateTo(entry.mLocation);
                       }
                   }, 150);
               }
            //}
        });

//        ImageView ivManeuver = (ImageView)convertView.findViewById(R.id.thumbnail);
//        ivManeuver.setImageBitmap(entry.mThumbnail);
//        entry.fetchThumbnailOnThread(ivManeuver);

        return convertView;
    }


}
