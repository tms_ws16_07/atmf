package pictures.taking.atmfinder.SettingsMenu;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.osmnavigator.MapActivity;

import java.util.HashMap;

import pictures.taking.atmfinder.R;


/**
 * An activity representing a list of Items.the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link ItemListFragment} and the item details
 * (if present) is a {@link ItemDetailFragment}.
 * <p/>
 * This activity also implements the required
 * {@link Callbacks} interface
 * to listen for item selections.
 */
public class ItemListActivity extends Activity implements Callbacks {
    protected static HashMap<String, String> hashMap = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        if (findViewById(R.id.item_detail_container) != null) {

            ((ItemListFragment) getFragmentManager()
                    .findFragmentById(R.id.item_list))
                    .setActivateOnItemClick(true);
        }

    }

    /**
     * Callback method from {@link Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(String id) {

        // show the detail view in this activity by
        // adding or replacing the detail fragment using a
        // fragment transaction.
        Bundle arguments = new Bundle();
        arguments.putString(ItemListFragment.STATE_ACTIVATED_POSITION, id);

        PrefsFragment fragment = new PrefsFragment();
        fragment.setArguments(arguments);

        getFragmentManager().beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit();


       /* FragmentManager mFragmentManager = getFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager
                .beginTransaction();
        PrefsFragment mPrefsFragment = new PrefsFragment();
        mFragmentTransaction.replace(R.id.item_detail_container, mPrefsFragment);
        mFragmentTransaction.commit();*/


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("map", hashMap);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
//        hashMap.clear();
    }


    public static class PrefsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener, Callbacks {


        private int id;

        public PrefsFragment() {
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);


            String menuID = getArguments().getString(ItemListFragment.STATE_ACTIVATED_POSITION);
            System.out.println(menuID);

            switch (menuID) {
                case MapActivity.KEY_PREF_ROUTE_PROVIDER:
                    addPreferencesFromResource(R.xml.pref_route_provider);
                    break;
                case MapActivity.KEY_PREF_TILE_PROVIDER:
                    addPreferencesFromResource(R.xml.pref_tile_provider);
                    break;
                case MapActivity.KEY_PREF_ABOUT:
                    addPreferencesFromResource(R.xml.pref_about);
                    break;

            }


            // Load the pref_cache_manager from an XML resource

        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            switch (key) {
                case (MapActivity.KEY_PREF_ROUTE_PROVIDER): {

                    Preference connectionPref = findPreference(key);

                    switch (Integer.valueOf(sharedPreferences.getString(key, ""))) {
                        case MapActivity.GRAPHHOPPER_FASTEST: {
                            hashMap.put("routing", "car");
                            break;
                        }
                        case MapActivity.GRAPHHOPPER_BICYCLE: {
                            hashMap.put("routing", "bike");

                            break;
                        }
                        case MapActivity.GRAPHHOPPER_PEDESTRIAN: {
                            hashMap.put("routing", "pedestrian");
                            break;
                        }
                        default: {
                            break;
                        }
                    }
//                connectionPref.setSummary(sharedPreferences.getString(key, ""));
                    break;
                }

                case (MapActivity.KEY_PREF_TILE_PROVIDER): {
                    Preference connectionPref = findPreference(key);
                    if (Boolean.parseBoolean(sharedPreferences.getString(key, ""))) {
                        hashMap.put("nightmap", "true");
                    } else {
                        hashMap.put("nightmap", "false");
                    }

                    break;
//                connectionPref.setSummary(sharedPreferences.getString(key, ""));
                }

                default: {
                    break;
                }

            }

        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);

        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);

        }


        @Override
        public void onItemSelected(String id) {

        }
    }

}
