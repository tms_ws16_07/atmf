package com.osmnavigator;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.osmdroid.bonuspack.R;
import org.osmdroid.bonuspack.location.POI;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow;

/**
 * A customized InfoWindow handling POIs.
 * We inherit from MarkerInfoWindow as it already provides most of what we want.
 * And we just add support for a "more info" button.
 *
 * @author M.Kergall
 */
public class POIATMInfoWindow extends MarkerInfoWindow {

    private Marker marker;
    private POI mSelectedPOI;
    private MapActivity mapActivity;
    TextView subDescText;

    public POIATMInfoWindow(MapView mapView, Activity mapAct) {
        super(R.layout.bonuspack_bubble, mapView);
        mapActivity = (MapActivity) mapAct;

//        Bitmap background = BitmapFactory.decodeResource(mapAct.getResources(), pictures.taking.atmfinder.R.drawable.popup_bg);
//        Drawable icon = new BitmapDrawable(mapAct.getResources(), Bitmap.createScaledBitmap(background, 50, 50, true));


        mView.setBackgroundResource(pictures.taking.atmfinder.R.drawable.popup_bg);
        mView.findViewById(R.id.bubble_moreinfo).setBackgroundResource(pictures.taking.atmfinder.R.drawable.route_me);
        subDescText = (TextView) mView.findViewById(org.osmdroid.bonuspack.R.id.bubble_subdescription);
        subDescText.setText("TESTADDRESS");


        final Button btn = (Button) (mView.findViewById(R.id.bubble_moreinfo));
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                //Set as new Destination
                MenuItem menuItem = new MenuItem() {
                    @Override
                    public int getItemId() {
                        return pictures.taking.atmfinder.R.id.menu_destination;
                    }

                    @Override
                    public int getGroupId() {
                        return 0;
                    }

                    @Override
                    public int getOrder() {
                        return 0;
                    }

                    @Override
                    public MenuItem setTitle(CharSequence charSequence) {
                        return null;
                    }

                    @Override
                    public MenuItem setTitle(int i) {
                        return null;
                    }

                    @Override
                    public CharSequence getTitle() {
                        return null;
                    }

                    @Override
                    public MenuItem setTitleCondensed(CharSequence charSequence) {
                        return null;
                    }

                    @Override
                    public CharSequence getTitleCondensed() {
                        return null;
                    }

                    @Override
                    public MenuItem setIcon(Drawable drawable) {
                        return null;
                    }

                    @Override
                    public MenuItem setIcon(int i) {
                        return null;
                    }

                    @Override
                    public Drawable getIcon() {
                        return null;
                    }

                    @Override
                    public MenuItem setIntent(Intent intent) {
                        return null;
                    }

                    @Override
                    public Intent getIntent() {
                        return null;
                    }

                    @Override
                    public MenuItem setShortcut(char c, char c1) {
                        return null;
                    }

                    @Override
                    public MenuItem setNumericShortcut(char c) {
                        return null;
                    }

                    @Override
                    public char getNumericShortcut() {
                        return 0;
                    }

                    @Override
                    public MenuItem setAlphabeticShortcut(char c) {
                        return null;
                    }

                    @Override
                    public char getAlphabeticShortcut() {
                        return 0;
                    }

                    @Override
                    public MenuItem setCheckable(boolean b) {
                        return null;
                    }

                    @Override
                    public boolean isCheckable() {
                        return false;
                    }

                    @Override
                    public MenuItem setChecked(boolean b) {
                        return null;
                    }

                    @Override
                    public boolean isChecked() {
                        return false;
                    }

                    @Override
                    public MenuItem setVisible(boolean b) {
                        return null;
                    }

                    @Override
                    public boolean isVisible() {
                        return false;
                    }

                    @Override
                    public MenuItem setEnabled(boolean b) {
                        return null;
                    }

                    @Override
                    public boolean isEnabled() {
                        return false;
                    }

                    @Override
                    public boolean hasSubMenu() {
                        return false;
                    }

                    @Override
                    public SubMenu getSubMenu() {
                        return null;
                    }

                    @Override
                    public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
                        return null;
                    }

                    @Override
                    public ContextMenu.ContextMenuInfo getMenuInfo() {
                        return null;
                    }

                    @Override
                    public void setShowAsAction(int i) {

                    }

                    @Override
                    public MenuItem setShowAsActionFlags(int i) {
                        return null;
                    }

                    @Override
                    public MenuItem setActionView(View view) {
                        return null;
                    }

                    @Override
                    public MenuItem setActionView(int i) {
                        return null;
                    }

                    @Override
                    public View getActionView() {
                        return null;
                    }

                    @Override
                    public MenuItem setActionProvider(ActionProvider actionProvider) {
                        return null;
                    }

                    @Override
                    public ActionProvider getActionProvider() {
                        return null;
                    }

                    @Override
                    public boolean expandActionView() {
                        return false;
                    }

                    @Override
                    public boolean collapseActionView() {
                        return false;
                    }

                    @Override
                    public boolean isActionViewExpanded() {
                        return false;
                    }

                    @Override
                    public MenuItem setOnActionExpandListener(OnActionExpandListener onActionExpandListener) {
                        return null;
                    }
                };


//                marker.closeInfoWindow();
                mapActivity.setDestinationPoint(mSelectedPOI.mLocation);

              /*  mapActivity.setMarkerDestination(mapActivity.updateItineraryMarker(mapActivity.getMarkerDestination(), mapActivity.getDestinationPoint(), mapActivity.DEST_INDEX,
                        pictures.taking.atmfinder.R.string.destination, pictures.taking.atmfinder.R.drawable.marker_poi, -1, null));*/
                mapActivity.setMarkerDestination(mapActivity.updateItineraryMarker(mapActivity.getMarkerDestination(), mapActivity.getDestinationPoint(), MapActivity.DEST_INDEX,
                        pictures.taking.atmfinder.R.string.destination, pictures.taking.atmfinder.R.drawable.marker_poi, -1, null));
                mapActivity.getRoadAsync();
                Toast.makeText(view.getContext(), pictures.taking.atmfinder.R.string.MapActivity_calculate_route, Toast.LENGTH_LONG).show();


            }
        });
    }

    @Override
    public void onOpen(Object item) {

        marker = (Marker) item;
        mSelectedPOI = (POI) marker.getRelatedObject();
        super.onOpen(item);
        subDescText.setText(mSelectedPOI.mAddress);

        //Fetch the thumbnail in background
//        if (mSelectedPOI.mThumbnailPath != null) {
//            ImageView imageView = (ImageView) mView.findViewById(R.id.bubble_image);
//            mSelectedPOI.fetchThumbnailOnThread(imageView);
//        }

        //show "set as new Destination button"
//        mView.findViewById(R.id.bubble_moreinfo).setBackgroundResource(R.drawable.route_me);
        mView.findViewById(R.id.bubble_moreinfo).setVisibility(View.VISIBLE);
        mView.findViewById(R.id.bubble_subdescription).setVisibility(View.VISIBLE);


        //Show or hide "more info" button:
//		if (mSelectedPOI.mUrl != null)
//			mView.findViewById(org.osmdroid.bonuspack.R.id.bubble_moreinfo).setVisibility(View.VISIBLE);
//		else
//			mView.findViewById(org.osmdroid.bonuspack.R.id.bubble_moreinfo).setVisibility(View.GONE);

    }
}
