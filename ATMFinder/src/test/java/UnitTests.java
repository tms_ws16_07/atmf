/**
 * Created by Sebastian on 19.01.2017.
 */
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import com.osmnavigator.MapActivity;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import pictures.taking.atmfinder.GPSTracker;

import static org.mockito.Mockito.when;

//import static org.mockito.Mockito.*;

public class UnitTests {
    @Mock
    MapActivity mockMapActivity;

    @Mock
    Location mockLocation;
    //@Mock
    //GPSTracker mockGPSTracker;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    protected GPSTracker gps;
    private LocationManager mockLm;
    private Context context;
    final double fakeLong = 33.5;
    final double fakeLat = 54.7;


    @Before
    public void setupMocks() {

        mockLm = Mockito.mock(LocationManager.class);
        //MockContext context = new MockContext();
        context = Mockito.mock(Context.class);


        when(mockLm.getLastKnownLocation(LocationManager.GPS_PROVIDER)).thenReturn(mockLocation);

        when(mockLm.isProviderEnabled(LocationManager.GPS_PROVIDER)).thenReturn(true);
        when(mockLm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)).thenReturn(true);


        when(context.getSystemService(Context.LOCATION_SERVICE)).thenReturn(mockLm);

    }

    @Test
    public void canGetLocationTest() {

        gps = new GPSTracker(context);

        Assert.assertTrue(gps.canGetLocation());
    }

    @Test
    public void getLongitude() {

        gps = new GPSTracker(context);

        Assert.assertEquals(0.0, gps.getLatitude());

        when(mockLocation.getLongitude()).thenReturn(fakeLong);

        Assert.assertEquals(fakeLong, gps.getLongitude());

    }

    @Test
    public void getLatitude() {

        gps = new GPSTracker(context);

        Assert.assertEquals(0.0, gps.getLatitude());

        when(mockLocation.getLatitude()).thenReturn(fakeLat);

        Assert.assertEquals(fakeLat, gps.getLatitude());

    }
}
