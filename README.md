# ATMFINDER

The ATMFINDER helps the user find the ATM witch is the nearest around him with his own spesifications.

[https://bitbucket.org/tms_ws16_07/atmf)


## Contact the Developers
- Maximilian Schmidt
- Sebastian Røed Mangseth
- Nils Machate


## Main Component Screenshots
![Mainscreen](http://www2.pic-upload.de/img/32550420/phpXIMjGyPM.jpg)
![itinearyscreen](http://www2.pic-upload.de/img/32550362/phpdWvn4JPM.jpg)
![settingsscreen](http://www2.pic-upload.de/img/32550409/phpWsTFvCPM.jpg)
 
### External libraries/frameworks/etc.

Projects being used in this app e.g. :

- Android API 23 Platform
- Java 1.8
- OSMBonusPack
- osmdroid-mapsforge:5.5:release@aar
- mapsforge-map-android:0.6.1
- mapsforge-map:0.6.1
- osmdroid-android:5.6.2
- support-v4:25.1.0
- design:25.1.0
- junit:4.12
- mockito-core:2.0.57-beta

##Permissions

- ACCESS_COARSE_LOCATION
- ACCESS_FINE_LOCATION
- ACCESS_WIFI_STATE
- ACCESS_NETWORK_STATE
- INTERNET
- WRITE_EXTERNAL_STORAGE


## Getting Started

Install of app:
1. Download android studio (https://developer.android.com/studio/index.html) 
2. Install Android studio
3. Download the project (https://bitbucket.org/tms_ws16_07/atmf) and store it in your chosen location.
4. Open the project from android studio (It should be marked as an android studio file) and wait for it to load.
5. Run the project in android studios on one of your chosen emulators

## Usecase
1. Max has been out on the town partying.
2. Max needs Money
2. Max opens ATMFINDER
3. Max looks at the route he can take to get to the closest ATM witch supports his Bankcard.
4. He looks at the Map but is seeing that the ATM is in the wrong direction.
5. He selects another ATM witch is in a more favorable Position for him.
6. He starts walking

## FAQ

## Testing


## Licence
No licence defined